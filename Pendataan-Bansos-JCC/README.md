NAMA    : I Gusti Glen Joyo Diningrat Wiyono
KELAS   : Vue.Js   


Link deploy di hosting (Github Pages/Netlify/Surge/Render).
https://subtle-dasik-1a10b5.netlify.app

1.Tentang Layout/Desain

Desain yang saya buat sangat sederhana, sedikit warna dan tidak memiliki pola, dengan harapan supaya mudah digunakan oleh orang awam. Desain sekilas mirip dengan google form namun saya modifikasi dibeberapa bagian dan saya gabungkan desain nya dengan desain default app dari Vuetify ,  dan saya hanya menambahkan gambar logo jabar Digital service di dalam bagian nav bar.

2.Kolom Input
Saya menggunakan jenis kolom input dengan posisi label berada diatas dan berukuran lebih kecil daripada Kolom Input, yang bertujuan supaya pengguna lebih mudah melihat mana kolom untuk diisi dan mana label.
Dalam mempermudah pengisian sama menggunakan placeholder yang berisikan contoh atau keterangan apa yang harus diisikan atau apa yang harus dilakukan pengguna untuk mengisi kolom tersebut.

3.Validasi Form
Dalam memvalidasi form saya menggunakan validasi langsung dan tidak langsung :

(A) Validasi langsung, validasi langsung yang mana ketika data yang diinput kurang tepat maka kolom akan merespon dan memberitahukan apa kesalahannya dibawah kolom input yang sedang aktif.

(B)Validasti tidak langsung, validasi ini saya gunakan ketika pengguna menekan tombol simpan. Sistem akan melakukan validasi terhadap seluruh kolom input apakah sudah sesuai atau belum dan jika salah akan menampilkan informasi kesalahannya dibawah kolom input yang tidak sesuai.





# pendataan-bansos

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
